import java.util.Scanner;

public class ArrayHelper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] zahlen = {1,2,3,4,5};
		System.out.print(convertArrayToString(umdrehen(zahlen)));
		temperaturTabelle(10);
		int[][] matrix = matrixAufbauen(5,5);
		ausgebenMatrix(matrix);
		if (istTransponiert(matrix)) {
			System.out.println("Die Matrix ist zu sich selber transponiert.");
		}
		else {
			System.out.println("Die Matrix ist nicht zu sich selber transponiert.");
		}
	}
	public static String convertArrayToString(int[] zahlen) {
		String ausgabe="";
		for (int i=0; i<zahlen.length;i++) {
			ausgabe+=zahlen[i];
			if (i!= zahlen.length-1)
				ausgabe+=", ";
		}
		return ausgabe;
	}
	public static int[] umdrehen(int[] eingabe) {
		int temp=0;
		for (int i=0;i<eingabe.length/2;i++) {
			temp=eingabe[i];
			eingabe[i]=eingabe[eingabe.length-i-1];
			eingabe[eingabe.length-i-1]=temp;
		}
		return eingabe;
	}
	public static int[] umdrehen2(int[] eingabe) {
		int[] ausgabe = new int[eingabe.length];
		for (int i=0;i<eingabe.length;i++) {
			ausgabe[eingabe.length-i-1]=eingabe[i];
		}
		return ausgabe;
	}
	public static float[][] temperaturTabelle(int anzSpalten){
		float[][] tabelle = new float[anzSpalten][2];
		for (int i=0; i<anzSpalten;i++) {
			tabelle[i][0]=i*10.0f;
			tabelle[i][1]=(tabelle[i][0]-32)*(5.0f/9.0f);
		}
		//nur Anzeige der Tabelle
		//System.out.println("");
		//System.out.println("Tabelle Fahrenheit zu Celsius");
		//for (int i=0;i<anzSpalten;i++) {
		//	System.out.printf("%-10.3f%s%10.3f\n",tabelle[i][0],"|",tabelle[i][1]);
		//}
		return tabelle;
	}
	public static int[][] matrixAufbauen(int anzZeilen, int anzSpalten){ //Aufgabe 5
		Scanner scanner = new Scanner(System.in);
		int [][] ausgabe = new int[anzZeilen][anzSpalten];
		System.out.println("Bitte geben sie die Zahl f�r das mit dem x ausgew�hlte Feld an.");
		for (int i=0; i<anzZeilen*anzSpalten; i++) {
			for (int y=0; y<anzZeilen; y++) {
				for (int x=0; x<anzSpalten; x++) {
					if (y*anzSpalten+x==i) {
						System.out.print(" x");
					}else System.out.print(" "+ausgabe[y][x]);
				}
				System.out.println("");
			}
			ausgabe[i/anzSpalten][i%anzSpalten]=scanner.nextInt();
			System.out.println("");
			System.out.println("");
		}
		return ausgabe;
	}
	public static void ausgebenMatrix(int[][]matrix) {
		for (int y=0; y<matrix.length; y++) {
			for (int x=0; x<matrix[0].length; x++) {
				System.out.printf("%-4d", matrix[y][x]);
			}
			System.out.println("");
		}
	}
	public static boolean istTransponiert(int[][]matrix) {
		boolean istTransponiert = true;
		for (int y=1; y<matrix.length; y++) {
			for (int x=0; x<matrix[0].length-y; x++) {
				if (matrix[matrix.length-x-1][y]!=matrix[y][matrix.length-x-1]) {
					istTransponiert=false;
				}
			}
		}
		return istTransponiert;
	}
}
