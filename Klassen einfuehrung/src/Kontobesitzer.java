
public class Kontobesitzer {
	//Attribute
	String name;
	String vorname;
	Konto[] konten;
	//Konstruktoren
	public Kontobesitzer() {
		this.konten = new Konto[2];
	}
	public Kontobesitzer(String name, String vorname) {
		super();
		this.name = name;
		this.vorname = vorname;
		this.konten = new Konto[2];
		this.konten[0]= new Konto();
		this.konten[1]= new Konto();
	}
	public Kontobesitzer(String name, String vorname, Konto k1, Konto k2) {
		super();
		this.name = name;
		this.vorname = vorname;
		this.konten = new Konto[2];
		this.konten[0] = k1;
		this.konten[1] = k2;
	}
	//Verwaltungsmethoden
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public Konto[] getKonten() {
		return konten;
	}
	public void setKonten(Konto[] konten) {
		this.konten = konten;
	}
	//Methoden
	public void gesamtUebersicht() {
		for (int i=0;i<2;i++) {
			System.out.printf("%s\n\t%-20s%s\n\t%-20s%s\n",(i+1)+". Konto:","Iban:",this.konten[i].getIban(),"Kontonummer:", this.konten[i].getKontonummer());
		}
	}
	public void gesamtGeld() {
		for (int i=0;i<2;i++) {
			System.out.printf("%s%20.2f\n",("Kontostand des "+(i+1)+". Kontos:"),this.konten[i].getKontostand());
		}
	}
}
