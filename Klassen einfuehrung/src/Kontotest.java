
public class Kontotest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Konto k1=new Konto();
		Konto k2=new Konto("DE10080", "00189072", 500);
		k2.ueberweisen(k1,200);
		System.out.println(k1.getKontostand()+"\t"+k2.getKontostand());
		Kontobesitzer kb1 = new Kontobesitzer();
		Kontobesitzer kb2 = new Kontobesitzer("Meier","Friedrich",k1,k2);
		kb2.gesamtGeld();
		kb2.gesamtUebersicht();
		k2.ueberweisen(k1,200);
		System.out.println(k1.getKontostand()+"\t"+k2.getKontostand());
		kb2.gesamtGeld();
	}

}
