package raumschiff;
import java.util.Random;
/**
 * 
 * @author Felix Heller
 * Die Klasse Raumschiff wird verwendet um Raumschiffe f�r ein Videospiel darzustellen
 */
public class Raumschiff {
	//Attribute
	Ladung[] ladungsverzeichnis;
	String name;
	double huelle;
	double schild;
	double energieversorgung;
	double lebenserhaltung;
	int reperaturAndroiden;
	int photonenTorpedos;
	Broadcastkommunikator broadcastkommunikator;
	//Konstruktoren
	public Raumschiff() {}
	
	public Raumschiff(Ladung[] ladungsverzeichnis, String name, double huelle, double schild, double energieversorgung, double lebenserhaltung, int reperaturAndroiden, int photonenTorpedos, Broadcastkommunikator broadcastkommunikator) {
		super();
		this.ladungsverzeichnis = ladungsverzeichnis;
		this.name = name;
		this.huelle = huelle;
		this.schild = schild;
		this.energieversorgung = energieversorgung;
		this.lebenserhaltung = lebenserhaltung;
		this.reperaturAndroiden = reperaturAndroiden;
		this.photonenTorpedos = photonenTorpedos;
		this.broadcastkommunikator = broadcastkommunikator;
	}

	//Verwaltungsmethoden (Getter und Setter)
	public Ladung[] getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(Ladung[] ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHuelle() {
		return huelle;
	}
	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}
	public double getSchild() {
		return schild;
	}
	public void setSchild(double schild) {
		this.schild = schild;
	}
	public double getEnergieversorgung() {
		return energieversorgung;
	}
	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	public double getLebenserhaltung() {
		return lebenserhaltung;
	}
	public void setLebenserhaltung(double lebenserhaltung) {
		this.lebenserhaltung = lebenserhaltung;
	}
	public int getReperaturAndroiden() {
		return reperaturAndroiden;
	}
	public void setReperaturAndroiden(int reperaturAndroiden) {
		this.reperaturAndroiden = reperaturAndroiden;
	}
	public int getPhotonenTorpedos() {
		return photonenTorpedos;
	}
	public void setPhotonenTorpedos(int photonenTorpedos) {
		this.photonenTorpedos = photonenTorpedos;
	}
	//Methoden
	/**
	 * Die mitgegebene Ladung wird in das Ladungsverzeichnis des Raumschiffs eingefuegt
	 * @param neueLadung Ladungsobjekt welches geladen werden soll
	 */
	private void addLadung(Ladung neueLadung) {
		boolean istVoll=true;
		for (int gege= 0;gege<this.ladungsverzeichnis.length;gege++) {
			if (this.ladungsverzeichnis[gege]==null) {
				istVoll=false;
				this.ladungsverzeichnis[gege]=neueLadung;
			}
		}
		if (istVoll) {
			Ladung[] temp = new Ladung[(this.ladungsverzeichnis.length +1)];
			for(int i=0; i<this.ladungsverzeichnis.length;i++) {
				temp[i]=this.ladungsverzeichnis[i];
			}
			temp[temp.length-1]=neueLadung;
			this.ladungsverzeichnis= new Ladung[temp.length];
			this.ladungsverzeichnis = temp;
		}
	}
	/**
	 * Die angegebene Ladung wird in das Raumschiff geladen entweder als neues Element in dem Ladungsverzeichnis oder indem die Menge einer Ladung des gleichen Typs erhoeht wird
	 * @param ladung Ladung die in das Raumschiff geladen werden soll
	 */
	public void belade(Ladung ladung) {
		boolean istGeladen=false;
		for (int q = 0;q<this.ladungsverzeichnis.length;q++) {
			if (this.ladungsverzeichnis[q].getTyp().equals(ladung.getTyp())) {
				this.ladungsverzeichnis[q].setAnzahl(this.ladungsverzeichnis[q].getAnzahl()+ladung.getAnzahl());
				istGeladen=true;
			}
		}
		if (!istGeladen) {
			this.addLadung(ladung);
		}
	}
	/**
	 * Vorraussetzung: Mindestens ein Photonentorpedo existiert,
	 * dann wird ein Torpedo aus dem Raumschiff entfernt und die Methode treffer bei dem Ziel aufgerufen,
	 * sonst wird ein Click in den Broadcastkommunikator geschrieben
	 * @param ziel Das Raumschiff welches abgeschossen werden soll.
	 */
	public void torpedoschuss(Raumschiff ziel) {
		if (this.photonenTorpedos >= 1 ) {
			this.photonenTorpedos-=1;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			ziel.treffer();
		}
		else {this.nachrichtAnAlle("-=*Click*=-");}
	}
	/**
	 * Vorraussetzung Energieversorgung ueber 50,
	 * dann wird die Energieversorgung vom Raumschiff um 50 reduziert und die Methode treffer bei dem Ziel aufgerufen,
	 * sonst wird ein Click in den Broadcastkommunikator geschrieben
	 * @param ziel Das Raumschiff welches abgeschossen werden soll.
	 */
	public void phaserkanonenschuss(Raumschiff ziel) {
		if (this.energieversorgung>50) {
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			ziel.treffer();
			this.setEnergieversorgung(this.getEnergieversorgung()-50);
		}
		else {this.nachrichtAnAlle("-=*Click*=-");}
	}
	/**
	 * Wenn Das Raumschiff einen Schild von ueber 0 hat wird der Schild um 50 reduziert, sollte der Schild unter 0 oder auf 0 fallen wird der Schild auf 0 gesetzt und von der huelle 50 abgezogen
	 * Wenn der Schild beim Aufruf der Methode 0 ist wird huelle um 50 reduziert
	 * wenn dann huelle auf 0 ist wird die Lebenserhaltung auf 0 gesetzt und auf dem Broadcastkommunikator eine Nachricht angehaengt
	 */
	public void treffer() {
		System.out.println(this.getName()+" wurde getroffen!");
		
		if (this.getSchild()>=0) {
			this.setSchild(this.getSchild()-50);
			if (this.getSchild()<=0) {
				this.setSchild(0);
				this.setHuelle(this.getHuelle()-50);
				this.setEnergieversorgung(this.getEnergieversorgung()-50);
				}
		}
		else {
			this.setHuelle(this.getHuelle()-50);
		}
		
		if (this.getHuelle()<=0) {
			this.setLebenserhaltung(0);
			this.nachrichtAnAlle("Die Lebenserhaltungssysteme des Raumschiffs "+ this.getName() +" wurden vollst�ndig zerst�rt.");
		}
	}
	/**
	 * Dem Broadcastkommunikator wird eine Nachricht angehangen, die den Namen des Raumschiffs und die uebergeben Nachricht enthaelt
	 * @param nachricht Nachricht die das Raumschiff dem Broadcastkommunikator anhaengt
	 */
	public void sendeNachricht(String nachricht) {
		this.nachrichtAnAlle("Das Raumschiff"+this.getName()+"sendet folgende Nachricht: "+nachricht);
	}
	/**
	 * Auf der Konsole werden der Name des Raumschiffs sowie die Werte f�r schild, huelle, energieversorgung, lebenserhaltung, photonentorpedos und reperaturandroiden ausgegeben. 
	 */
	public void zustand() {
		System.out.printf("%s\n\t%-30s%s\n\t%-30s%s\n\t%-30s%s\n\t%-30s%s\n\t%-30s%s\n\t%-30s%s\n", "Status des Raumschiffs:"+ this.getName(),"Schild:",this.getSchild(),"Huelle:",this.getHuelle(),"Energieversorgung",this.getEnergieversorgung(),"Lebenserhaltungssysteme:",this.getLebenserhaltung(),"Anzahl der Photonentorpedos:",this.getPhotonenTorpedos(),"Anzahl der Reperaturandroiden:",this.getReperaturAndroiden());
	}
	/**
	 * Auf der Konsole wird von jedem Ladungsobjekt in dem Ladungsverzeichnis der Typ und die Menge ausgegeben.
	 */
	public void printLadungsverzeichnis() {
		System.out.println("Dies ist das Ladungsverzeichnis von dem Raumschiff "+this.getName());
		for (int i = 0; i<this.ladungsverzeichnis.length;i++) {
			if (this.ladungsverzeichnis[i]!=null) {
				System.out.println(this.ladungsverzeichnis[i].toString());
			}
		}
	}
	/**
	 * Schild und huelle werden entsprechend der Eingabeparameter um einen Zufallswert erhoeht der durch die Menge an eigesetzen Androiden und der Menge an Reperaturstellen(Schild, Huelle) beinflusst wird
	 * @param anz Angabe wieviele Reperaturandroiden verwendet werden sollen
	 * @param huelle boolscher Wert bei true wird die huelle mit repariert
	 * @param schild boolscher Wert bei true wird der Schild mit repariert
	 */
	public void benutzeReperaturandroiden(int anz, boolean huelle, boolean schild) {
		Random random = new Random();
		int zufall = random.nextInt(101)-1;
		int zuReparieren = 0;
		int reparaturMenge = 0;
		int anzahl=anz;
		if (anzahl > this.getReperaturAndroiden()) {
			anzahl = this.getReperaturAndroiden();
		}
		if (huelle) {zuReparieren+=1;}
		if (schild) {zuReparieren+=1;}
		if (zuReparieren !=0) {
			reparaturMenge = (zufall*anzahl)/zuReparieren;
		}
		if (huelle) {
			this.setHuelle(this.getHuelle()+reparaturMenge);
		}
		if (schild) {
			this.setSchild(this.getSchild()+reparaturMenge);
		}
	}
	/**
	 * Es wird durch das Ladungsverzeichnis iteriert und Ladungen mit einer Menge von 0 werden entfernt
	 */
	public void ladungsverzeichnisAufraeumen() {
		for (int i=0;i<this.ladungsverzeichnis.length;i++) {
			if (this.ladungsverzeichnis[i].getAnzahl()==0) {
				this.ladungsverzeichnis[i]=null;
			}
		}
	}
	/**
	 * Dem Broadcastkommunikator wird ein String angehangen
	 * @param nachricht String der dem Broadcastkommunikator angehangen werden soll
	 */
	public void nachrichtAnAlle(String nachricht) {
		this.broadcastkommunikator.eintragHinzufuegen(nachricht);
	}
	/**
	 * Die Nachrichten im Broadcastkommunikator werden ausgelesen
	 * @return String der alle Nachrichten aus dem Broadcastkommunikator enthaelt
	 */
	public String logsAusgeben() {
		return("---------------------------------\n"
				+ "-------------Logbuch-------------\n"
				+ this.broadcastkommunikator.getLogEintraege()
				+ "---------------------------------\n");
	}
	/**
	 * Wenn eine Ladung mit dem typ Photonentorpedo im Ladungsverzeichnis ist wird diese um die Anzahl reduziert und das Attribut Photonentorpedos wird um die Anzahl erhoeht
	 * ist die angegebene Anzahl niedrieger als die geladene dann werden alle geladenen Phtotentorpedos stattdessen geladen indem die Menge auf 0 gesetzt wird und das Attribut Photonentorpedos um die geladene Anzahl erhoeht wird
	 * @param anz Integer Wert wieviele Torpedos geladen werden sollen
	 */
	public void torpedosNachladen(int anz) {
		boolean torpedoVorhanden = false;
		for (int i = 0; i<this.ladungsverzeichnis.length;i++) {
			if (this.ladungsverzeichnis[i].getTyp().equals("Photonentorpedo")) {
				torpedoVorhanden = true;
				if (this.ladungsverzeichnis[i].getAnzahl()>=anz) {
					this.photonenTorpedos+=anz;
					this.ladungsverzeichnis[i].setAnzahl(this.ladungsverzeichnis[i].getAnzahl()-anz);
				}
				else {
					this.photonenTorpedos=this.ladungsverzeichnis[i].getAnzahl();
					this.ladungsverzeichnis[i].setAnzahl(0);
				}
			}
		}
		if (!torpedoVorhanden) {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
}
