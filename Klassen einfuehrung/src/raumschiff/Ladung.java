package raumschiff;
/**
 * @author Felix HelLer
 * 
 * Die Ladungsklasse kann von Raumschiffen transportiert werden und hat je einen Typ und eine Menge.
 */
public class Ladung {
	//attribute
	private String typ;
	private int anzahl;
	//Konstruktoren
	public Ladung() {}
	public Ladung(String typ, int anzahl) {
		super();
		this.typ = typ;
		this.anzahl = anzahl;
	}
	//Verwaltungsmethoden
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	//anderer Methoden
	/**
	 * Die Methode gibt sowohl typ als auch Menge der Ladung an
	 * @return Rueckgabewert ist ein Sring in dem die Attribute "typ" und "anzahl" enthalten sind
	 */
	public String toString() {
		return ("Dies ist eine Ladung von "+this.typ +" und es sind "+ this.anzahl+ " Exemplare davon geladen.");
	}
}
