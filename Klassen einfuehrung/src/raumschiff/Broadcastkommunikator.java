package raumschiff;
/**
 * 
 * @author Felix Heller
 * Stellt einen gemeinsamen Broadcastkommunikator f�r alle Raumschiffe bereit
 *
 */
public class Broadcastkommunikator {
	//Attribute
	private String logEintraege;
	//Konstruktoren
	public Broadcastkommunikator() {
		this.logEintraege = "";
	}
	//Verwaltungsmethoden
	public String getLogEintraege() {
		return logEintraege;
	}
	//andere Methoden
	/**
	 * der uebergebene String wird an das Attribut logEintraege mit einem Zeilenumbruch angefuegt
	 * @param eintrag String der angehangen werden soll
	 */
	public void eintragHinzufuegen(String eintrag) {
		this.logEintraege+=eintrag+"\n";
	}
	
}
