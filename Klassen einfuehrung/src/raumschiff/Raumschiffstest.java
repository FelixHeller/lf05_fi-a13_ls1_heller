package raumschiff;

/**
 * 
 * @author Felix Heller
 * Dies ist die Testklasse in der die Objekte erzeugt und die Methoden ausgefuehrt werden
 *
 */
public class Raumschiffstest {
	/**
	 * Main-Methode wird zum ausfuehren von Code ohne dem Erzeugen eines Objekts benoetigt.
	 */
	public static void main(String[] args) {
		// Erzeugen der Objekte
		Ladung l1 = new Ladung("Ferengi Schneckensaft",200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasmawaffe", 50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		Broadcastkommunikator bc = new Broadcastkommunikator();
		
		Ladung[] klingonen_ladung = new Ladung[2];
		klingonen_ladung[0]=l1;
		klingonen_ladung[1]=l5;
		Raumschiff klingonen = new Raumschiff(klingonen_ladung, "IKS Hegh'ta", 100, 100, 100, 100, 2, 1, bc);
		Ladung[] romulaner_ladung = new Ladung[3];
		romulaner_ladung[0] = l2;
		romulaner_ladung[1] = l3;
		romulaner_ladung[2] = l6;
		Raumschiff romulaner = new Raumschiff(romulaner_ladung, "IRW Khazara", 100, 100, 100, 100, 2, 2, bc);
		Ladung[] vulkanier_ladung = new Ladung[2];
		vulkanier_ladung[0] = l4;
		vulkanier_ladung[1] = l7;
		Raumschiff vulkanier = new Raumschiff(vulkanier_ladung, "Ni'Var", 100, 100, 100, 100, 5, 0, bc);
		
		
		// Hier beginnen die Methodenaufrufe
		
		klingonen.torpedoschuss(romulaner);
		romulaner.phaserkanonenschuss(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustand();
		klingonen.printLadungsverzeichnis();
		vulkanier.benutzeReperaturandroiden(5, true, true);
		vulkanier.torpedosNachladen(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.torpedoschuss(romulaner);
		klingonen.torpedoschuss(romulaner);
		klingonen.zustand();
		klingonen.printLadungsverzeichnis();
		System.out.println("----------------------------------------------------");
		romulaner.zustand();
		romulaner.printLadungsverzeichnis();
		System.out.println("----------------------------------------------------");
		vulkanier.zustand();
		vulkanier.printLadungsverzeichnis();
		System.out.println("----------------------------------------------------");
		System.out.println(bc.getLogEintraege());
		
	}

}
