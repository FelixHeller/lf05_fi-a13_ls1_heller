
public class Konto {
	private String iban;
	private String kontonummer;
	private double kontostand;
	//Konstruktoren
	public Konto() {}
	public Konto(String iban, String kontonummer, double stand) {
		this.iban=iban;
		this.kontonummer=kontonummer;
		this.kontostand=stand;
	}
	//Getter und Setter
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getKontonummer() {
		return kontonummer;
	}
	public void setKontonummer(String kontonummer) {
		this.kontonummer = kontonummer;
	}
	public double getKontostand() {
		return kontostand;
	}
	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}
	//restliche Methoden
	public void abheben(double betrag) {
		this.kontostand-=betrag;
	}
	public void einzahlen(double betrag) {
		this.kontostand+=betrag;
	}
	public void ueberweisen(Konto ziel, double betrag) {
		this.kontostand -= betrag;
		ziel.setKontostand(ziel.getKontostand()+betrag);
		
	}
}
