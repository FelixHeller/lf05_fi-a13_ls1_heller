import java.awt.EventQueue;
import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class FormAendern {

	private JFrame frame;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormAendern window = new FormAendern();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FormAendern() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 438, 684);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblDieserTextSoll.setBounds(12, 30, 396, 16);
		panel.add(lblDieserTextSoll);
		
		JLabel lblAufgabe = new JLabel("Hintergrundfarbe \u00E4ndern:");
		lblAufgabe.setBounds(12, 83, 152, 16);
		panel.add(lblAufgabe);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(12, 112, 124, 25);
		panel.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(148, 112, 124, 25);
		panel.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(284, 112, 124, 25);
		panel.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(12, 153, 124, 25);
		panel.add(btnGelb);
		
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandard.setBounds(148, 153, 124, 25);
		panel.add(btnStandard);
		
		JButton btnFarbewaehlen = new JButton("Farbe w\u00E4hlen");
		btnFarbewaehlen.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(JColorChooser.showDialog(null, "Farbauswahl", null));
			}
		});
		btnFarbewaehlen.setBounds(284, 153, 124, 25);
		panel.add(btnFarbewaehlen);
		
		JLabel lblAufgabe_1 = new JLabel("Text formatieren:");
		lblAufgabe_1.setBounds(12, 191, 124, 16);
		panel.add(lblAufgabe_1);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Arial",lblDieserTextSoll.getFont().getStyle(),lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnArial.setBounds(12, 220, 124, 25);
		panel.add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Comic Sans MS",lblDieserTextSoll.getFont().getStyle(),lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnComicSans.setBounds(148, 220, 124, 25);
		panel.add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font("Courier New",lblDieserTextSoll.getFont().getStyle(),lblDieserTextSoll.getFont().getSize()));
			}
		});
		btnCourierNew.setBounds(284, 220, 124, 25);
		panel.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(12, 258, 396, 22);
		panel.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(12, 293, 192, 25);
		panel.add(btnInsLabelSchreiben);
		
		JButton btnLabellschen = new JButton("Text im Label l\u00F6schen");
		btnLabellschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnLabellschen.setBounds(216, 293, 192, 25);
		panel.add(btnLabellschen);
		
		JLabel lblSchriftfarbendern = new JLabel("Schriftfarbe \u00E4ndern");
		lblSchriftfarbendern.setBounds(12, 331, 152, 16);
		panel.add(lblSchriftfarbendern);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(12, 360, 124, 25);
		panel.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(148, 360, 124, 25);
		panel.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(284, 360, 124, 25);
		panel.add(btnSchwarz);
		
		JLabel lblSchriftgrendern = new JLabel("Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblSchriftgrendern.setBounds(12, 398, 124, 16);
		panel.add(lblSchriftgrendern);
		
		JButton btnGrosser = new JButton("+");
		btnGrosser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font(lblDieserTextSoll.getFont().getFontName(),lblDieserTextSoll.getFont().getStyle(),lblDieserTextSoll.getFont().getSize()+1));
			}
		});
		btnGrosser.setBounds(12, 427, 192, 25);
		panel.add(btnGrosser);
		
		JButton btnKleiner = new JButton("-");
		btnKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setFont(new Font(lblDieserTextSoll.getFont().getFontName(),lblDieserTextSoll.getFont().getStyle(),lblDieserTextSoll.getFont().getSize()-1));
			}
		});
		btnKleiner.setBounds(216, 427, 192, 25);
		panel.add(btnKleiner);
		
		JLabel lblTextausrihtung = new JLabel("Textausrichtung");
		lblTextausrihtung.setBounds(12, 470, 124, 16);
		panel.add(lblTextausrihtung);
		
		JButton btnLinks = new JButton("linksb\u00FCndig");
		btnLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(JLabel.LEFT);
			}
		});
		btnLinks.setBounds(12, 499, 124, 25);
		panel.add(btnLinks);
		
		JButton btnCenter = new JButton("zentriert");
		btnCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(JLabel.CENTER);
			}
		});
		btnCenter.setBounds(148, 499, 124, 25);
		panel.add(btnCenter);
		
		JButton btnRechts = new JButton("rechtsb\u00FCndig");
		btnRechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(JLabel.RIGHT);
			}
		});
		btnRechts.setBounds(284, 499, 124, 25);
		panel.add(btnRechts);
		
		JLabel lblProgrammBeenden = new JLabel("Programm beenden");
		lblProgrammBeenden.setBounds(12, 537, 152, 16);
		panel.add(lblProgrammBeenden);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(12, 566, 396, 58);
		panel.add(btnExit);
	}
}
