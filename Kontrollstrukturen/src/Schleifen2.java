import java.util.Scanner;
public class Schleifen2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben sie eine Zahl an:");
		int n = scanner.nextInt();
		zaehlen(n);
		herunterzaehlen(n);
	}
	public static void zaehlen(int n) {
		int i = 1;
		while (n >= i){
			System.out.println(i);
			i++;
		}
	}
	public static void herunterzaehlen(int n) {
		while (n>0) {
			System.out.println(n);
			n--;
		}
	}

}
