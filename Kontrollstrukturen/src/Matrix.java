import java.util.Scanner;
public class Matrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl von 2-9 an: ");
		matrix(scanner.nextInt());
	}
	public static void matrix(int z) {
		int aktuelle_zahl=0;
		int quersumme=0;
		boolean ist_teilbar=false;
		boolean ist_quersumme=false;
		boolean ist_inZahl=false;
		
		for (int x=0; x<=9; x++) {
			for (int y=0; y<=9; y++) {
				aktuelle_zahl=(x*10+y);
				quersumme=x+y;
				
				//Booleans festlegen
				ist_teilbar=aktuelle_zahl%z==0;
				ist_quersumme=quersumme==z;
				ist_inZahl=x==z||y==z;
				
				if (aktuelle_zahl==0) 
					System.out.printf("%3s",aktuelle_zahl);
				else if (ist_teilbar||ist_quersumme||ist_inZahl)
					System.out.printf("%3s","*");
				else
					System.out.printf("%3s",aktuelle_zahl);
			}
			System.out.print("\n");
		}
	}
}
