import java.util.Scanner;
public class quadrat {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		System.out.print("geben Sie die Seitenlaenge des Quadrats an: ");
		quadrat_zeichnen(scanner.nextInt());
	}
	public static void quadrat_zeichnen(int n) {
		for (int y=0;y<n;y++) {
			for (int x=0;x<n;x++) {
				if (y==0||y==n-1) {
					System.out.print("* ");
				}
				else if (x==0||x==n-1) {
					System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.print("\n");
		}
	}
}
