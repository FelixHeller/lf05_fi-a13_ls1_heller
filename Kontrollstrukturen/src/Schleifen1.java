import java.util.Scanner;
public class Schleifen1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		zaehlen();
		runterzaehlen();
		summe();
		modulo();
	}
	public static void zaehlen() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ende der Z�hlung: ");
		int n = scanner.nextInt();
		for (int i = 0; i <= n; i++) {
			System.out.println(i);
		}
	}
	public static void runterzaehlen() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Anfang des Countdown: ");
		int n = scanner.nextInt();
		for (int i = n; i > 0; i--) {
			System.out.println(i);
		}
	}
	public static void summe() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Grenze der Summierung: ");
		int n = scanner.nextInt();
		int summe1 = 0;
		int summe2 = 0;
		int summe3 = 0;
		for (int i=0;i<=n;i++) {
			summe1+=i;
			if(i%2==0) {
				summe2+=i;
			}
			else {
				summe3+=i;
			}
		}
		System.out.println("Die Summe f�r A betraegt: "+ summe1);
		System.out.println("Die Summe f�r B betraegt: "+ summe2);
		System.out.println("Die Summe f�r C betraegt: "+ summe3);
	}
	public static void modulo() {
		for (int i = 0; i<=200; i++) {
			if(i%7==0 && i%5!=0 && i%4==0) {
				System.out.println(i);
			}
		}
	}
}
