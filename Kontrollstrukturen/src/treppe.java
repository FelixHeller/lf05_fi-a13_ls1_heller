import java.util.Scanner;
public class treppe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		System.out.println("Geben Sie die breite der Treppe an: ");
		int breite= scanner.nextInt();
		System.out.println("Geben Sie die hoehe der Treppe an: ");
		int hoehe= scanner.nextInt();
		treppe_bauen(breite,hoehe);
	}
	public static void treppe_bauen(int breite, int hoehe) {
		int maximale_breite=hoehe*breite;
		String stufe="";
		String ausgabe;
		for (int h=1;h<=hoehe;h++) {	
			ausgabe="%"+maximale_breite+'s'+"\n";
			for (int b=0;b<breite;b++) {
				stufe=stufe+"*";
			}
			System.out.printf(ausgabe, stufe);
		}
	}

}
