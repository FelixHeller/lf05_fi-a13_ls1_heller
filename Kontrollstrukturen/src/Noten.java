import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("geben Sie eine Note als Zahl ein");
		byte note = scanner.nextByte();
		switch (note) {
		case 1:
			System.out.println("1 bedeutet sehr gut");
			break;
		case 2:
			System.out.println("2 bedeutet gut");
			break;
		case 3:
			System.out.println("3 bedeutet befriedigend");
			break;
		case 4:
			System.out.println("4 bedeutet ausreichend");
			break;
		case 5:
			System.out.println("5 bedeutet mangelhaft");
			break;
		case 6:
			System.out.println("6 bedeutet ungenügend");
			break;
		default:
			System.out.println("Fehler!!!!");
		}
	}

}
