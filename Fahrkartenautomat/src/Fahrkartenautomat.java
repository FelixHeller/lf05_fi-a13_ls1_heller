﻿import java.util.Scanner;

class Fahrkartenautomat
{

    public static void main(String[] args){
    	
    	double zuZahlenderBetrag;
    	double rueckgabeBetrag;
    	while (true) {
    	zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(rueckgabeBetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       System.out.print("-----------------------------------------------------\n\n\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double[] ticketPreise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	String[] ticketSorten = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	System.out.println("Bitte wählen sie aus welches Ticket Sie kaufen wollen (Nummer eingeben).");
    	for (int i=0; i<ticketPreise.length; i++) {
    		System.out.printf("%-5d%-3s%-37s%-3s%5.2f\n", i+1,"|",ticketSorten[i],"|",ticketPreise[i]);
    	}
    	boolean eingabe_korrekt = true;
    	do {
    	int eingabe = tastatur.nextInt();
    	if (eingabe<1||eingabe>10) {
    		System.out.println("Ihre Eingabe ist nicht gültig. Bitte wählen sie ein gültiges Ticket.");
    		eingabe_korrekt = false;
    	}
    	else {
    		zuZahlenderBetrag+=(ticketPreise[eingabe-1]*100); //ich rechne hier den Geldbetrag in Cent um damit intern kein Rundungsfehler bei dem Rückgabebetrag passiert.
    	}
    	
    	}while (eingabe_korrekt==false);
    	System.out.print("\nWieviele Fahrkarten dieser Art wollen Sie bestellen:");
    	zuZahlenderBetrag = tastatur.nextInt() * zuZahlenderBetrag;
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen (double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	float eingeworfeneMuenze;
        double rueckgabebetrag;
        double eingezahlterGesamtbetrag;
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f\n","Noch zu zahlen: ",(zuZahlenderBetrag/100 - eingezahlterGesamtbetrag/100));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMuenze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMuenze*100;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	
    	if(rueckgabebetrag > 0.0)
        {
     	   //System.out.println("Der Rueckgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
     	   System.out.printf("%s%.2f%s\n","Der Rückgabebetrag in Höhe von ",rueckgabebetrag/100," EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 200.0) // 2 EURO-Muenzen
            {
         	  System.out.println("2 EURO");
 	          rueckgabebetrag -= 200.0;
            }
            while(rueckgabebetrag >= 100.0) // 1 EURO-Muenzen
            {
         	  System.out.println("1 EURO");
 	          rueckgabebetrag -= 100.0;
            }
            while(rueckgabebetrag >= 50.0) // 50 CENT-Muenzen
            {
         	  System.out.println("50 CENT");
 	          rueckgabebetrag -= 50.0;
            }
            while(rueckgabebetrag >= 20.0) // 20 CENT-Muenzen
            {
         	  System.out.println("20 CENT");
  	          rueckgabebetrag -= 20.0;
            }
            while(rueckgabebetrag >= 10.0) // 10 CENT-Muenzen
            {
         	  System.out.println("10 CENT");
 	          rueckgabebetrag -= 10.0;
            }
            while(rueckgabebetrag >= 5.0)// 5 CENT-Muenzen
            {
         	  System.out.println("5 CENT");
  	          rueckgabebetrag -= 5.0;
            }
        }
    }
}


/*DUrch die Realisierung mit Arrays ist die Berechnung des zuZahlenden Preises einfacher, da man einen direkten zugriff auf Preis vornehmen kann,
 * Dieser ermöglicht es einem die switch-case Verzweigung wegzulassen. Außerdem ist es Angenehmer die die Tickets und die Presise zu ändern.*/

